import { createSlice } from "@reduxjs/toolkit";

export const modalSlice = createSlice({
	name: "modal",
	initialState: {
		value: null,
	},
	reducers: {
		open: (state, action) => {
			state.value = action.payload;
		},
		close: (state) => {
			state.value = null;
		},
	},
});

export const { open, close } = modalSlice.actions;

export const selectModal = (state) => state.modal.value;

export default modalSlice.reducer;
