import EditNote from "./components/EditNote";
import Header from "./components/Header";
import Notes from "./components/Notes";
import Sidebar from "./components/Sidebar";

function App() {
	return (
		<div className="app">
			<Header />

			<div className="flex">
				<Sidebar />

				<Notes />

				<EditNote />
			</div>
		</div>
	);
}

export default App;
