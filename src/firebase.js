import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
	apiKey: "AIzaSyDDUDeTQCcrLixGGqOG6Y6kYsWybtI9-lE",
	authDomain: "keep-clone-9de63.firebaseapp.com",
	projectId: "keep-clone-9de63",
	storageBucket: "keep-clone-9de63.appspot.com",
	messagingSenderId: "246173104092",
	appId: "1:246173104092:web:e6b5e7bd34b363bd858119",
};

// Initialize Firebase
initializeApp(firebaseConfig);
const db = getFirestore();

export { db };
