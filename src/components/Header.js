import {
	Apps,
	Menu,
	Refresh,
	SettingsOutlined,
	ViewStreamOutlined,
} from "@mui/icons-material";
import { Avatar, IconButton, TextField } from "@mui/material";
import React from "react";
import logo from "../images/logo.png";

function Header() {
	return (
		<div className="flex items-center justify-between px-5 py-2 shadow-sm">
			<div className="flex items-center flex-1">
				<IconButton>
					<Menu />
				</IconButton>
				<img className="h-10 mx-3" src={logo} alt="" />
				<div className="ml-10 flex-grow max-w-lg">
					<TextField
						className="bg-gray-100"
						label="Search"
						fullWidth
						variant="outlined"
					/>
				</div>
			</div>

			<div className="flex items-center text-gray-500">
				<IconButton>
					<Refresh />
				</IconButton>
				<IconButton>
					<ViewStreamOutlined />
				</IconButton>
				<IconButton>
					<SettingsOutlined />
				</IconButton>
				<IconButton>
					<Apps />
				</IconButton>
				<Avatar />
			</div>
		</div>
	);
}

export default Header;
