import { collection, onSnapshot, orderBy, query } from "firebase/firestore";
import React, { useEffect, useState } from "react";
import { db } from "../firebase";
import NewNote from "./NewNote";
import Note from "./Note";

function Notes() {
	const [notes, setNotes] = useState([]);

	useEffect(
		() =>
			onSnapshot(
				query(collection(db, "notes"), orderBy("lastEdited", "desc")),
				(snapshot) => {
					setNotes(snapshot.docs);
				}
			),
		[]
	);

	return (
		<div className="flex-1">
			<NewNote />
			<div className="flex flex-wrap mx-10">
				{notes.map((note) => (
					<Note
						key={note.id}
						id={note.id}
						title={note.data().title}
						note={note.data().note}
					/>
				))}
			</div>
		</div>
	);
}

export default Notes;
