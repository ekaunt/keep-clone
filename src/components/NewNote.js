import {
	AddAlertOutlined,
	ArchiveOutlined,
	ImageOutlined,
	MoreVert,
	PaletteOutlined,
	PersonAddOutlined,
} from "@mui/icons-material";
import { Button, IconButton, TextField } from "@mui/material";
import React, { useState } from "react";
import { addDoc, collection, serverTimestamp } from "firebase/firestore";
import { db } from "../firebase";

function NewNote() {
	const [title, setTitle] = useState("");
	const [note, setNote] = useState("");

	const newNote = async () => {
		await addDoc(collection(db, "notes"), {
			title: title,
			note: note,
			lastEdited: serverTimestamp(),
		});

		setTitle("");
		setNote("");
	};

	return (
		<div className="flex flex-col p-3 space-y-3 mx-3 my-7 shadow-lg border rounded-lg">
			<TextField
				value={title}
				onChange={(e) => setTitle(e.target.value)}
				placeholder="Title"
				variant="standard"
			/>
			<TextField
				value={note}
				onChange={(e) => setNote(e.target.value)}
				multiline
				placeholder="Take a note…"
				variant="standard"
			/>
			<div className="flex justify-between items-center mt-3">
				<div className="flex items-center">
					<IconButton>
						<AddAlertOutlined />
					</IconButton>
					<IconButton>
						<PersonAddOutlined />
					</IconButton>
					<IconButton>
						<PaletteOutlined />
					</IconButton>
					<IconButton>
						<ImageOutlined />
					</IconButton>
					<IconButton>
						<ArchiveOutlined />
					</IconButton>
					<IconButton>
						<MoreVert />
					</IconButton>
				</div>
				<Button className="!text-gray-500" onClick={newNote}>
					Save
				</Button>
			</div>
		</div>
	);
}

export default NewNote;
