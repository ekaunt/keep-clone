import {
	AddAlertOutlined,
	ArchiveOutlined,
	ImageOutlined,
	MoreVert,
	PaletteOutlined,
	PersonAddOutlined,
} from "@mui/icons-material";
import { IconButton } from "@mui/material";
import { deleteDoc, doc } from "firebase/firestore";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { db } from "../firebase";
import { open } from "../state/modalSlice";

function Note({ id, title, note }) {
	const [hover, setHover] = useState(false);
	const dispatch = useDispatch();

	const deleteNote = async () => {
		await deleteDoc(doc(db, "notes", id));
	};

	return (
		<div
			className="flex flex-col max-w-xs justify-between border rounded-lg hover:shadow-md p-3 m-1 transition-all duration-200 ease-out cursor-default"
			onMouseOver={() => setHover(true)}
			onMouseLeave={() => setHover(false)}
		>
			<div className="" onClick={() => dispatch(open(id))}>
				<p className="font-bold text-xl">{title}</p>
				<p className="text-lg whitespace-pre-wrap">{note}</p>
			</div>

			<div
				className={`items-center transition-opacity duration-200 ${
					!hover && "opacity-0"
				}`}
			>
				<IconButton>
					<AddAlertOutlined />
				</IconButton>
				<IconButton>
					<PersonAddOutlined />
				</IconButton>
				<IconButton>
					<PaletteOutlined />
				</IconButton>
				<IconButton>
					<ImageOutlined />
				</IconButton>
				<IconButton>
					<ArchiveOutlined onClick={deleteNote} />
				</IconButton>
				<IconButton>
					<MoreVert />
				</IconButton>
			</div>
		</div>
	);
}

export default Note;
