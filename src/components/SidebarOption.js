import React from "react";

function SidebarOption({ Icon, title, selected }) {
	return (
		<div
			className={`flex items-center py-3 pl-4 cursor-pointer flex-1 rounded-tr-full rounded-br-full hover:bg-gray-200 ${
				selected && "bg-yellow-100"
			}`}
		>
			<Icon />
			<p className="pl-4">{title}</p>
		</div>
	);
}

export default SidebarOption;
