import {
	ArchiveOutlined,
	CreateOutlined,
	DeleteOutlined,
	LightbulbOutlined,
	NotificationsOutlined,
} from "@mui/icons-material";
import React from "react";
import SidebarOption from "./SidebarOption";

function Sidebar() {
	return (
		<div className="w-1/4">
			<SidebarOption Icon={LightbulbOutlined} title="Notes" selected />
			<SidebarOption Icon={NotificationsOutlined} title="Reminders" />
			<SidebarOption Icon={CreateOutlined} title="Edit Labels" />
			<SidebarOption Icon={ArchiveOutlined} title="Archive" />
			<SidebarOption Icon={DeleteOutlined} title="Trash" />
		</div>
	);
}

export default Sidebar;
